Each client must have different address, so before launching second and every following client you must modify remote_app.conf.
Simply change hostname = "127.0.0.1" to, e.g. hostname = "127.0.0.2" etc.

Sample data is simple:
	in "databases" title and price are separated with colon: <title>:<price>, e.g. Ogniem i mieczem:35
	orders.txt simply contains titles of ordered books. 
	
I forgot on schema about arrows indicating init