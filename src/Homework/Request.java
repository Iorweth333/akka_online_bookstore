package Homework;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Request implements Serializable {
    private RequestType requestType;
    private char [] content;

    public Request (RequestType requestType, String content) {
        this.requestType = requestType;
        this.content = content.toCharArray();
    }

    public Request (RequestType requestType, char[] content) {
        this.requestType = requestType;
        this.content = content;
    }

    public Request (String content) {
        this.content = content.toCharArray();
        this.requestType = RequestType.UNDEFINED;
    }

    public Request(){
        this.requestType = RequestType.INIT;
    }

    public void setTypeToSearch (){
        this.requestType = RequestType.SEARCH;
    }

    public void setTypeToOrder(){
        this.requestType = RequestType.ORDER;
    }

    public void setTypeToStream(){
        this.requestType = RequestType.STREAM;
    }

    public RequestType getRequestType () {
        return requestType;
    }

    public void setRequestType (RequestType requestType) {
        this.requestType = requestType;
    }

    public char[] getContent () {
        return content;
    }

    public void setContent (char[] content) {
        this.content = content;
    }

    private void readObject (ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
    }

    private void writeObject (ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
    }
}
