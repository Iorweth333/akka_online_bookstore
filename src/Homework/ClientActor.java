package Homework;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

@SuppressWarnings("Duplicates")

public class ClientActor extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    String supervisorPath = "akka.tcp://remote_system@127.0.0.1:3552/user/supervisor";
    String directory = "akka.tcp://remote_system@127.0.0.1:3552/user/";
    String path;

    @Override
    public AbstractActor.Receive createReceive () {
        return receiveBuilder()
                .match(String.class, s -> {
                    String title;
                    Request request;
                    if (s.startsWith("search")) {
                        title = s.substring("search".length() + 1);
                        title = title.trim();
                        request = new Request (RequestType.SEARCH, title);
                        getContext().actorSelection(path).tell(request, getSelf());
                    } else if (s.startsWith("order")){
                        title = s.substring("order".length() + 1);
                        title = title.trim();
                        request = new Request (RequestType.ORDER, title);
                        getContext().actorSelection(path).tell(request, getSelf());
                    } else if (s.startsWith("stream")){
                        title = s.substring("stream".length() + 1);
                        title = title.trim();
                        request = new Request (RequestType.STREAM, title);
                        getContext().actorSelection(path).tell(request, getSelf());
                    } else if (s.startsWith("name")){
                        String remoteName = s.substring(s.indexOf(":") + 1);   //if name is aaa then this string is name:aaa
                        path = directory + remoteName;
                        System.out.println("Ready to work (server actor: " + remoteName + ")");
                    }
                    else {
                        System.out.println(s);
                    }
                })
                .match(Integer.class, price -> {
                    if (price != -1)
                        System.out.println("Price: " + price);
                    else
                        System.out.println("Title not found");
                })
                .match(Request.class, r -> {
                    getContext().actorSelection(supervisorPath).tell(r, getSelf());
                })
                .matchAny(o -> {
                    log.info("Received unknown message.");
                })
                .build();
    }


}
