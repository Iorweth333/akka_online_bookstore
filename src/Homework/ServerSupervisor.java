package Homework;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.DeciderBuilder;
import scala.concurrent.duration.Duration;

import static akka.actor.SupervisorStrategy.restart;
import static akka.actor.SupervisorStrategy.resume;

public class ServerSupervisor extends AbstractActor {
    // for logging
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private static int count = 0;

    @Override
    public AbstractActor.Receive createReceive () {
        return receiveBuilder()
                .match(Request.class, request -> {
                    count++;
                    ActorRef remote = this.getContext().getSystem().actorOf(Props.create(ServerActor.class));   //HUGE TODO when does ServerActor die?
                    String name = remote.path().name();
                    getSender().tell("name:" + name, getSelf());
                })
                .matchAny(o -> {
                    log.info("Received unknown message.");
                })
                .build();
    }

    private static SupervisorStrategy strategy
            = new OneForOneStrategy(10, Duration.create("1 minute"), DeciderBuilder.
                    matchAny(e -> restart()).
                    build());

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }
}
