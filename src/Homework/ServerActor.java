package Homework;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class ServerActor extends AbstractActor {
    private  static final Integer lock = 0;

    // for logging
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    String ordersPath = "sample_data/orders.txt";
    String db1Path = "sample_data/db1.txt";
    String db2Path = "sample_data/db2.txt";

    int [] results = new int [2];

    // must be implemented -> creates initial behaviour
    @Override
    public AbstractActor.Receive createReceive () {
        return receiveBuilder()
                .match(Request.class, request -> {
                    String requestedTitle = new String(request.getContent());
                    switch (request.getRequestType()){
                        case ORDER:
                            synchronized (ServerActor.lock) {
                                FileWriter writer = new FileWriter(ordersPath, true);
                                BufferedWriter buffer = new BufferedWriter(writer);
                                buffer.write(request.getContent());
                                buffer.write("\n");
                                buffer.close();
                            }
                            log.info("New order added successfully.");
                            getSender().tell("Order completed", getSelf());
                            break;
                        case STREAM:
                            log.error("Not implemented yet");
                            throw new Exception("Not implemented yet");
//                            break;
                        case SEARCH:
                            Runnable r1 = new DBSearcher(requestedTitle, db1Path, results, 0);
                            Thread db1searcher =  new Thread(r1);
                            db1searcher.start();
                            Runnable r2 = new DBSearcher(requestedTitle, db2Path, results, 1);
                            Thread db2searcher =  new Thread(r2);
                            db2searcher.start();
                            db1searcher.join();
                            db2searcher.join();
                            //instead of threads, i should have used actors
                            //preferably one actor (SearchSupervisor) who spawns new actors (Searchers) for each database
                            //when SearchSUpervisor would get answer from any child it would kill/ignore/terminate other children (XD)
                            //and immediately send it here
                            Integer price;
                            if (results[0] > results[1])    //the assumption is that databases (if both work) are consistent- that is if the title is in both databases, the prices are the same, and if only in one, the other returns -1
                                price = results[0];
                            else
                                price = results[1];

                            getSender().tell(price, getSelf());
                            log.info("Search ended, result: " + price);
                            break;
                        default:
                            log.info("Undefined request");
                    }
                })
                .matchAny(o -> {
                    log.info("Received unknown message.");
                })
                .build();
    }


}
