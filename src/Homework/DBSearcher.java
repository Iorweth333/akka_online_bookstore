package Homework;

import java.io.*;

public class DBSearcher implements Runnable {
    String searchFor;
    String path;
    int[] results;
    int myPosition;

    public DBSearcher (String searchFor, String path, int[] results, int myPosition) {
        this.searchFor = searchFor;
        this.path = path;
        this.results = results;
        this.myPosition = myPosition;
    }

    //Looks for a specified title in a database under specified path and returns it's price as int in the given array under provided index


    @Override
    public void run () {
        File file = new File(path);
        //The teacher pointed out that proper synchronization of accessing the database should be taken care of
        //I assume he meant opening the file in proper mode, i.e. read only
        //but AFAIK java does not have something like opening the file to read only (like e.g. c++ where you can do
        //fstream f;
        //f.open("file", fstream::in);
        //I've done some research and i've read that java doesnt put any locks- the system does
        //So I assume that just using BufferedReader is enough and leave below code commented
//        while (!file.setReadOnly()) {  //try to make it readonly as long as it takes
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                //ignore
//            }
//        }
        System.out.println("Started searching for: " + searchFor);
        int price = -1;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null && price == -1) {
                if (st.startsWith(searchFor)) {
                    System.out.println(st);
                    int priceStartsAt = st.indexOf(';') + 1;
                    String priceString = st.substring(priceStartsAt);
                    price = Integer.parseInt(priceString);
                    System.out.println("Title found!");
                }
            }
        } catch (Exception e) {
            System.out.println("Exception occurred");
            e.printStackTrace();
            price = -1;
        }

        try {
            Thread.sleep(5000);             //to simulate long search
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        results[myPosition] = price;

//        while (!file.setWritable(true)) {  //try to make it writable again as long as it takes
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                //ignore
//            }
//        }

    }
}
