package Homework;

import Lab.Z1_MathActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

@SuppressWarnings("Duplicates")

public class Client {
    public static void main (String[] args) throws Exception {

        // config
        File configFile = new File("remote_app.conf");
        Config config = ConfigFactory.parseFile(configFile);

        // create actor system & actors
        final ActorSystem system = ActorSystem.create("local_system", config);
        final ActorRef actor = system.actorOf(Props.create(ClientActor.class), "clientActor");
        final Materializer materializer = ActorMaterializer.create(system);
        System.out.println("Started. Commands: 'order <name>', 'search <name>', stream <name>, 'q'");

        //init
        actor.tell(new Request(), null);

        // read line & send to actor
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line = br.readLine();
            if (line.equals("q")) {
                break;
            }
            actor.tell(line, null);     // send message to actor
        }

        // finish
        system.terminate();
    }
}
